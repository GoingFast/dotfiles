source "%val{config}/plugins/plug.kak/rc/plug.kak"

plug "andreyorst/plug.kak" noload
plug "dracula/kakoune" theme
plug "alexherbo2/prelude.kak"
plug "lePerdu/kakboard" %{
    hook global WinCreate .* %{ kakboard-enable }
}
plug "alexherbo2/search-highlighter.kak"
plug "alexherbo2/manual-indent.kak" %{
	hook global WinCreate .* %{
		manual-indent-enable
	}
	
	hook global WinSetOption filetype=.* %{
		manual-indent-remove-filetype-hooks
	}
}

plug "alexherbo2/surround.kak" %{
	map global user S ': enter-user-mode surround<ret>' 
}

plug "alexherbo2/toggle-highlighter.kak" %{
	map global user L ': toggle-highlighter global/number-lines number-lines<ret>' -docstring 'Toggle number-lines highlighter'
	map global user W ': toggle-highlighter global/wrap wrap -word<ret>' -docstring 'Toggle wrap highlighter'
}

plug "alexherbo2/word-select.kak" config %{
    map global normal q ': word-select-previous-word<ret>'
    map global normal <a-q> ': word-select-previous-big-word<ret>'
    map global normal Q B
    map global normal <a-Q> <a-B>        
    map global normal w ': word-select-next-word<ret>'
    map global normal <a-w> ': word-select-next-big-word<ret>'
}

plug "alexherbo2/move-line.kak" config %{
    map global normal <down> ': move-line-below<ret>'
    map global normal <up> ': move-line-above<ret>'
}

plug "alexherbo2/phantom.kak"
plug "alexherbo2/auto-pairs.kak"
plug 'delapouite/kakoune-cd' %{
    alias global cdb change-directory-current-buffer
    alias global cdr change-directory-project-root
    alias global ecd edit-current-buffer-directory
    alias global pwd print-working-directory
}

plug "https://gitlab.com/Screwtapello/kakoune-state-save" %{
	hook global KakBegin .* %{
    	state-save-reg-load colon
        state-save-reg-load pipe
        state-save-reg-load slash
	}
	hook global KakEnd .* %{
    	state-save-reg-save colon
	    state-save-reg-save pipe
    	state-save-reg-save slash
	}
}

plug "alexherbo2/split-object.kak" %{
	map global normal <a-I> ': enter-user-mode split-object<ret>'
}


plug "ul/kak-lsp" do %{
    cargo install --locked --force --path .
} config %{
	map global user l ": enter-user-mode<space>lsp<ret>" -docstring "enable lsp mode for next key"
    hook global WinSetOption filetype=(go|c|cpp|python) %{
    	set-option window lsp_hover_anchor false
    	lsp-enable-window
    	lsp-auto-signature-help-enable
    }
}
plug "rubberydub/nord-kakoune" theme
plug "TeddyDD/kakoune-selenized" theme

plug "alexherbo2/auto-pairs.kak" config %{
	map global user S -docstring 'Surround' ': auto-pairs-surround <lt> <gt><ret>'
	hook global WinSetOption filetype=(go|c|cpp|python) %{
		auto-pairs-enable
	}
}

plug "occivink/kakoune-vertical-selection"
plug "Delapouite/kakoune-text-objects" %{
    text-object-map
}
plug "Delapouite/kakoune-buffers" config %{
    map global normal ^ q
    map global normal <a-^> Q

    map global normal b ':enter-buffers-mode<ret>'              -docstring 'buffers'
    map global normal B ':enter-user-mode -lock buffers<ret>'   -docstring 'buffers (lock)'
    map global buffers w ":write<ret>" -docstring "write buffer"
}

plug "alexherbo2/prelude.kak"
plug "alexherbo2/connect.kak" config %{
    require-module connect-nnn
    require-module connect-fzf
    require-module connect-broot
    declare-user-mode connect
    
    map global user c ":enter-user-mode connect<ret>" -docstring "enable connect mode for next key"
    map global connect n ":nnn<ret>" -docstring "open nnn"
    map global connect f ":fzf-files<ret>" -docstring "browse files using fzf"
    map global connect b ":broot<ret>" -docstring "open broot"
}
plug "occivink/kakoune-sudo-write"
plug "occivink/kakoune-find"

colorscheme tomorrow-night

set-option global ui_options ncurses_set_title=no ncurses_status_on_top=true
set-option global autoreload yes
set-option global tabstop 2
set-option global scrolloff 5,5
set-option global indentwidth 2
set-option global grepcmd "rg --column --with-filename --smartcase"

set-option global modelinefmt '{{mode_info}} in {green}%val{bufname}{default} {{context_info}} at {cyan}%val{cursor_line}{default}:{cyan}%val{cursor_char_column}{default} (client {magenta}%val{client}{default} on session {yellow}%val{session}{default})'

alias global qa quit
alias global qa! quit!
alias global wqa write-all-quit
alias global wq write-quit
alias global wq! write-quit!

define-command find-edit -params 1 -shell-script-candidates 'fd --type file' 'edit %arg(1)'
define-command find-edit-all -params 1 -shell-script-candidates 'fd --no-ignore --type file' 'edit %arg(1)'

alias global f find-edit
alias global fa find-edit-all

map global user f ":find-edit<space>" -docstring "find-edit"

map global insert <a-o> "<a-;>o<esc>"
map global insert <a-O> <esc>O
map global normal '#'  :comment-line<ret>
map global prompt <a-i> '<home>(?i)<end>'
map global prompt <a-.> '%sh(dirname "$kak_buffile")<a-!>'
map global normal <a-percent> '*%s<ret>'

map global normal <c-t> ': connect-terminal<ret>'
map global normal <c-n> ': connect-shell alacritty<ret>'

declare-user-mode window
map global normal <C-w> ": enter-user-mode<space>window<ret>" -docstring "enter window mode for next key"
map global window v ":vsplit<ret>" -docstring "vertical split"
map global window s ":split<ret>" -docstring "split"
map global window t ":tabnew<ret>" -docstring "new tab"

add-highlighter global/number-lines number-lines
add-highlighter global/show-matching show-matching


map global normal <ret> :
map global normal <backspace> ';'
map global normal <tab> '<a-;>'
map global normal <a-tab> '<a-:>'

hook global InsertChar k %{ try %{
  execute-keys -draft hH <a-k>jk<ret> d
  execute-keys <esc>
}}

hook global WinDisplay .* info-buffers

hook global BufWritePost .*\.go$ %{
    go-format -use-goimports
}

define-command -docstring "vsplit [<commands>]: split tmux vertically" \
vsplit -params .. -command-completion %{
    tmux-terminal-horizontal kak -c %val{session} -e "%arg{@}"
}

define-command -docstring "split [<commands>]: split tmux horizontally" \
split -params .. -command-completion %{
    tmux-terminal-vertical kak -c %val{session} -e "%arg{@}"
}

define-command -docstring "tabnew [<commands>]: create new tmux window" \
tabnew -params .. -command-completion %{
    tmux-terminal-window kak -c %val{session} -e "%arg{@}"
}
